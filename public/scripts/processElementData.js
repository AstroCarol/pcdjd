//------------------------------------------------------------------------------
// Copyright (c) 2005, 2006 IBM Corporation and others.
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v1.0
// which accompanies this distribution, and is available at
// http://www.eclipse.org/legal/epl-v10.html
// 
// Contributors:
// IBM Corporation - initial implementation
//------------------------------------------------------------------------------

// This file will include dynamically generated process layout related data from 
// the publishing service 
contentPage.processPage.imageFiles["_Hw9e8eOpEem-xfw8UR9g7w,_7IyoEeOpEem-xfw8UR9g7wActivity"]="./../../pcdjd/deliveryprocesses/resources/Elaboração_DE2CE3A2_cd7d0c78_Activity.jpeg"
contentPage.processPage.imageFiles["_Hw9e8eOpEem-xfw8UR9g7wActivity"]="./../../pcdjd/deliveryprocesses/resources/Ciclo-de-vida-pcdjd_24048082_24048082_Activity.jpeg"
contentPage.processPage.imageFiles["_Hw9e8eOpEem-xfw8UR9g7w,_BANHYeOqEem-xfw8UR9g7wActivity"]="./../../pcdjd/deliveryprocesses/resources/Teste_B9416AC0_a8919396_Activity.jpeg"
contentPage.processPage.imageFiles["_Hw9e8eOpEem-xfw8UR9g7w,_-PIG4eOpEem-xfw8UR9g7wActivity"]="./../../pcdjd/deliveryprocesses/resources/Brainstorming_567AD3EC_45cafcc2_Activity.jpeg"
contentPage.processPage.imageFiles["_Hw9e8eOpEem-xfw8UR9g7w,_L1KUMeOpEem-xfw8UR9g7w,_XFPhUeOpEem-xfw8UR9g7wActivity"]="./../../pcdjd/deliveryprocesses/resources/a_A0293210_34f0c11b_Activity.jpeg"
contentPage.processPage.imageFiles["_Hw9e8eOpEem-xfw8UR9g7w,_L1KUMeOpEem-xfw8UR9g7wActivity"]="./../../pcdjd/deliveryprocesses/resources/Desenvolvimento_5AFACCE1_4a4af5b7_Activity.jpeg"
contentPage.processPage.elementUrls["_Y8FmoeX4EemHLtQ61xtxOw"] = ["Desenvolvedor","pcdjd/deliveryprocesses/desenvolvedor_6A05C52.html"];
contentPage.processPage.elementUrls["_Hw9e8eOpEem-xfw8UR9g7w"] = ["Processo Específico de Desenvolvimento de Jogos Digitais - PEDJD","pcdjd/deliveryprocesses/Ciclo-de-vida-pcdjd_24048082.html"];
contentPage.processPage.elementUrls["_EVqnU-OqEem-xfw8UR9g7w"] = ["Definir história do jogo","pcdjd/deliveryprocesses/definir_historia_do_jogo_D0597721.html"];
contentPage.processPage.elementUrls["_z69IYuOpEem-xfw8UR9g7w"] = ["Desenvolver sistema de pontuação","pcdjd/deliveryprocesses/desenvolver_sistema_de_pontucao_1A15F2C2.html"];
contentPage.processPage.elementUrls["_EVqnXuOqEem-xfw8UR9g7w"] = ["Desenvolvedor","pcdjd/deliveryprocesses/desenvolvedor_CDD58DC6.html"];
contentPage.processPage.elementUrls["_I9vlteOqEem-xfw8UR9g7w"] = ["Designer","pcdjd/deliveryprocesses/designer_DAC3F052.html"];
contentPage.processPage.elementUrls["_I9vls-OqEem-xfw8UR9g7w"] = ["Desenvolvedor","pcdjd/deliveryprocesses/desenvolvedor_71097DFB.html"];
contentPage.processPage.elementUrls["_I9vlseOqEem-xfw8UR9g7w"] = ["Definir equipe","pcdjd/deliveryprocesses/definir_equipe_16320E33.html"];
contentPage.processPage.elementUrls["_EVqnUeOqEem-xfw8UR9g7w"] = ["Definir controles","pcdjd/deliveryprocesses/definir_controles_75820759.html"];
contentPage.processPage.elementUrls["_EVqnWeOqEem-xfw8UR9g7w"] = ["Definir universo do jogo","pcdjd/deliveryprocesses/definir_universo_do_jogo_FEA5CB97.html"];
contentPage.processPage.elementUrls["_Y8G0weX4EemHLtQ61xtxOw"] = ["Roterista","pcdjd/deliveryprocesses/roterista_1137F8AC.html"];
contentPage.processPage.elementUrls["_H5FOUSJhEeqWqOmlj44JSA"] = ["Desenvolvedor","pcdjd/deliveryprocesses/desenvolvedor_BB5180A2.html"];
contentPage.processPage.elementUrls["_z69IYOOpEem-xfw8UR9g7w"] = ["Desenvolver sistema de controle do player","pcdjd/deliveryprocesses/desenvolver_sistema_de_controle_do_player_20DEFE9C.html"];
contentPage.processPage.elementUrls["_XFPhUeOpEem-xfw8UR9g7w"] = ["Iteração","pcdjd/deliveryprocesses/a_A0293210.html"];
contentPage.processPage.elementUrls["_EVqnWuOqEem-xfw8UR9g7w"] = ["Descrever cutscenes","pcdjd/deliveryprocesses/descrever_cutscenes_943ABA7.html"];
contentPage.processPage.elementUrls["_EVqnUuOqEem-xfw8UR9g7w"] = ["Definir cronograma de desenvolvimento","pcdjd/deliveryprocesses/definir_cronograma_de_desenvolvimento_801FE769.html"];
contentPage.processPage.elementUrls["_z69IZuOpEem-xfw8UR9g7w"] = ["Selecionar/Desenhar cenário","pcdjd/deliveryprocesses/selecionardesenhar_cenario_DEA7D4E1.html"];
contentPage.processPage.elementUrls["_Dst7kSJhEeqWqOmlj44JSA"] = ["Iteração","pcdjd/deliveryprocesses/Iteração_E9EAD508.html"];
contentPage.processPage.elementUrls["_H5FOUCJhEeqWqOmlj44JSA"] = ["Realizar Play Teste","pcdjd/deliveryprocesses/realizar_play_teste_B0B3A092.html"];
contentPage.processPage.elementUrls["_EVqnYOOqEem-xfw8UR9g7w"] = ["Designer","pcdjd/deliveryprocesses/designer_99307BBF.html"];
contentPage.processPage.elementUrls["_I9vlsuOqEem-xfw8UR9g7w"] = ["Definir ideia","pcdjd/deliveryprocesses/definir_ideia_20CFEE43.html"];
contentPage.processPage.elementUrls["_H5FOVSJhEeqWqOmlj44JSA"] = ["Roterista","pcdjd/deliveryprocesses/roterista_7FE362C1.html"];
contentPage.processPage.elementUrls["_-PIG4eOpEem-xfw8UR9g7w"] = ["Brainstorming","pcdjd/deliveryprocesses/Brainstorming_567AD3EC.html"];
contentPage.processPage.elementUrls["_I9vlsOOqEem-xfw8UR9g7w"] = ["Criar esboço da ideia","pcdjd/deliveryprocesses/criar_esboco_da_ideia_2798FA1D.html"];
contentPage.processPage.elementUrls["_BANHYeOqEem-xfw8UR9g7w"] = ["Teste","pcdjd/deliveryprocesses/Teste_B9416AC0.html"];
contentPage.processPage.elementUrls["_H5FOUyJhEeqWqOmlj44JSA"] = ["Designer","pcdjd/deliveryprocesses/designer_B48874C8.html"];
contentPage.processPage.elementUrls["_z69IZeOpEem-xfw8UR9g7w"] = ["Implementar trilha sonora","pcdjd/deliveryprocesses/implementar_trilha_sonora_D409F4D1.html"];
contentPage.processPage.elementUrls["_EVqnVeOqEem-xfw8UR9g7w"] = ["Definir interface","pcdjd/deliveryprocesses/definir_interface_3A13E978.html"];
contentPage.processPage.elementUrls["_EVqnVOOqEem-xfw8UR9g7w"] = ["Definir inimigos","pcdjd/deliveryprocesses/definir_inimigos_4B7AD562.html"];
contentPage.processPage.elementUrls["_EVqnWOOqEem-xfw8UR9g7w"] = ["Definir título do jogo","pcdjd/deliveryprocesses/definir_titulo_do_jogo_100CB781.html"];
contentPage.processPage.elementUrls["_L1KUMeOpEem-xfw8UR9g7w"] = ["Desenvolvimento","pcdjd/deliveryprocesses/Desenvolvimento_5AFACCE1.html"];
contentPage.processPage.elementUrls["_EVqnW-OqEem-xfw8UR9g7w"] = ["Descrever gameplay","pcdjd/deliveryprocesses/descrever_gameplay_597D3B5F.html"];
contentPage.processPage.elementUrls["_z69IY-OpEem-xfw8UR9g7w"] = ["Implementar colisões","pcdjd/deliveryprocesses/implementar_colisoes_6A4F827A.html"];
contentPage.processPage.elementUrls["_z69IZ-OpEem-xfw8UR9g7w"] = ["Selecionar/Desenhar personagens","pcdjd/deliveryprocesses/selecionardesenhar_personagens_2EE16499.html"];
contentPage.processPage.elementUrls["_z69IYeOpEem-xfw8UR9g7w"] = ["Desenvolver sistema de mapas e fases","pcdjd/deliveryprocesses/desenvolver_sistema_de_mapas_e_fases_F7812B2.html"];
contentPage.processPage.elementUrls["_I9vlt-OqEem-xfw8UR9g7w"] = ["Roterista","pcdjd/deliveryprocesses/roterista_359B601A.html"];
contentPage.processPage.elementUrls["_EVqnXOOqEem-xfw8UR9g7w"] = ["Roterista","pcdjd/deliveryprocesses/roterista_D49E99A0.html"];
contentPage.processPage.elementUrls["_z69IZOOpEem-xfw8UR9g7w"] = ["Implementar Inimigos","pcdjd/deliveryprocesses/implementar_inimigos_E570E0BB.html"];
contentPage.processPage.elementUrls["_H5FOVyJhEeqWqOmlj44JSA"] = ["Jogo","pcdjd/deliveryprocesses/jogo_791A56E7.html"];
contentPage.processPage.elementUrls["_EVqnVuOqEem-xfw8UR9g7w"] = ["Definir personagens","pcdjd/deliveryprocesses/definir_personagens_44B1C988.html"];
contentPage.processPage.elementUrls["_7IyoEeOpEem-xfw8UR9g7w"] = ["Elaboração","pcdjd/deliveryprocesses/Elaboração_DE2CE3A2.html"];
contentPage.processPage.elementUrls["_EVqnUOOqEem-xfw8UR9g7w"] = ["Definir câmera do jogo","pcdjd/deliveryprocesses/definir_camera_do_jogo_86E8F343.html"];
contentPage.processPage.elementUrls["_EVqnYuOqEem-xfw8UR9g7w"] = ["Game Design Document","pcdjd/deliveryprocesses/game_design_document_92676FE5.html"];
contentPage.processPage.elementUrls["_Y8DxcOX4EemHLtQ61xtxOw"] = ["Designer","pcdjd/deliveryprocesses/designer_E83F3555.html"];
